describe('iLabs Application Process', function (){
  it('should follow the application process', function(){

    cy.visit('https://www.ilabquality.com/careers/')
    Cypress.on('uncaught:exception', (err, runnable) => {

  return false
})
    cy.get('.pum-content > .vc_row > :nth-child(2) > .vc_column-inner > :nth-child(1) > .vc_btn3-container > .vc_general').click()
      .get('#menu-item-1373 > a').click()
      .get(':nth-child(9) > .vc_general').click()
      .get(':nth-child(1) > .wpjb-col-main > .wpjb-line-major > .wpjb-job_title').click()
      .get(':nth-child(1) > .wpjb-button').click()
      .get('#applicant_name').type('Simphiwe Sithole')
      .get('#email').type('automationAssessment@iLABQuality.com')
      .get('#phone').type(Create_Random_Number())
        function Create_Random_Number() {
        var text = "";
        var possible = "122 345 6789";

        for (var i = 0; i < 10; i++)
        text = Math.floor(Math.random() * 9000000000) + 1000000000;
      return text;}
    cy.get('#wpjb_submit').click()
      .get('.wpjb-errors > li').should('have.text', 'You need to upload at least one file.')
  })
})
