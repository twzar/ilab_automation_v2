const starwars_characters = [
    {name: 'Anakin Skywalker', age: 40},
    {name: 'Obi Wan Kenobi', age: 60},
    {name: 'Luke Skywalker', age: 17}
]

console.log(starwars_characters.every(jedi => jedi.age > 18))
