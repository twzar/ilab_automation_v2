## Install Cypress

Visit **https://docs.cypress.io/guides/getting-started/installing-cypress.html#yarn-add** and follow the installation process/instructions for you Operating system.

---

## Clone this Project (bitbucket)

git clone https://simphiwes@bitbucket.org/twzar/ilab_automation_v2.git

---

## Running the tests

You can run either one of the following commands to open cypress: **npx cypress open** or **yarn run cypress open**. Once cypress is open you can got to the following folder **cypress/integration/iLab_Aplication.js** on the UI to run the tests.
This will open a chrome window to run the automated tests. Unfortunately there is only support for chrome for now but cypress is working on adding more browsers.

If you prefer running the tests from the terminal and want to see the ui running the tests(headless) you can run the following command **npx cypress run**. This way of running the tests gives you a more comprehensive report of the test run. For more information got to **https://docs.cypress.io/guides/guides/command-line.html#How-to-run-commands**.

## Pipeline Runs

You can also run the tests on the built in pipelines by bitbucket. You click on **Pipelines** from menu items on the left and thereafter click on **Run pipeline** from the menu items at the top. This should kick off the tests in a headless fashion.

## Reports

You can find reports in the **mochawesome-report** folder and you can view the html version from **mochawesome.html** or the **json** version from the **.json versions** within the same folder.
